<?php

namespace App\Http\Controllers;

use App\Kategori;
use App\Barang as model_barang;
use Illuminate\Http\Request;

class Barang extends Controller
{
    public function InputBarang(){
        $kategori = Kategori::get();

        return view('barang/input_barang', compact('kategori'));
    }

    public function ProsesInput(Request $request){
        $data = [
            'id_kategori' => $request->id_kategori,
            'nama_barang' => $request->nama_barang,
            'jumlah' => $request->jumlah,
            'satuan' => $request->satuan,
            'harga_beli' => $request->harga_beli,
            'harga_jual' => $request->harga_jual,
        ];

        model_barang::create($data);

        return redirect()->to('/barang');
    }

    public function UbahBarang(model_barang $barang){
        $kategori = Kategori::get();

        return view('barang/ubah_barang', compact('barang', 'kategori'));
    }

    public function ProsesUbah(Request $request, model_barang $barang){
        $data = [
            'id_kategori' => $request->id_kategori,
            'nama_barang' => $request->nama_barang,
            'jumlah' => $request->jumlah,
            'satuan' => $request->satuan,
            'harga_beli' => $request->harga_beli,
            'harga_jual' => $request->harga_jual,
        ];

        $barang->update($data);

        return redirect()->to('/barang');
    }

    public function ProsesHapus(model_barang $barang){
        $barang->delete();

        return redirect()->to('/barang');
    }
}
