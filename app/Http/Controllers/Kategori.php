<?php

namespace App\Http\Controllers;

use App\Kategori as model_kategori;
use Illuminate\Http\Request;

class Kategori extends Controller
{
    public function InputKategori(){

        return view('kategori/input_kategori');
    }

    public function ProsesInput(Request $request){
        $data = [
            'nama_kategori' => $request->nama_kategori,
            'keterangan' => $request->keterangan
        ];

        model_kategori::create($data);

        return redirect()->to('/kategori');
    }

    public function UbahKategori(model_kategori $kategori){

        return view('kategori/ubah_kategori', compact('kategori'));
    }

    public function ProsesUbah(Request $request,model_kategori $kategori){
        $data = [
            'nama_kategori' => $request->nama_kategori,
            'keterangan' => $request->keterangan
        ];

        $kategori->update($data);

        return redirect()->to('/kategori');
    }

    public function ProsesHapus(model_kategori $kategori){
        $kategori->delete();

        return redirect()->to('/kategori');
    }

}
