<?php

namespace App\Http\Controllers;

use App\Masuk;
use App\Keluar;
use App\Barang;
use App\Kategori;
use App\Supplier;
use App\Pelanggan;
use Illuminate\Http\Request;

class MyInventory extends Controller
{
    public function index(){
        $barang = Barang::get();
        $supplier = Supplier::get();
        $pelanggan = Pelanggan::get();
        $masuk = Masuk::get();
        $keluar = Keluar::get();

        return view('dashboard', compact('barang','supplier','pelanggan','masuk','keluar'));
    }

    public function barang(){
        $barang = Barang::join('kategori', 'barang.id_kategori', '=', 'kategori.id_kategori')->get();

        return view('barang/barang', compact('barang'));
    }

    public function supplier(){
        $supplier = Supplier::get();

        return view('supplier/supplier', compact('supplier'));
    }

    public function pelanggan(){
        $pelanggan = Pelanggan::get();

        return view('pelanggan/pelanggan', compact('pelanggan'));
    }

    public function keluar(){
        $keluar = Keluar::join('tujuan', 'keluar.id_tujuan', '=', 'tujuan.id_tujuan')->join('barang', 'keluar.id_barang', '=', 'barang.id_barang')->get();
        $pelanggan = Pelanggan::get();
        $barang = Barang::get();

        return view('transaksi/keluar', compact('pelanggan','barang','keluar'));
    }

    public function masuk(){
        $masuk = Masuk::join('supplier', 'masuk.id_supplier', '=', 'supplier.id_supplier')->join('barang', 'masuk.id_barang', '=', 'barang.id_barang')->get();
        $supplier = Supplier::get();
        $barang = Barang::get();

        return view('transaksi/masuk', compact('supplier','barang','masuk'));
    }

    public function kategori(){
        $kategori = Kategori::get();

        return view('kategori/kategori', compact('kategori'));
    }
}
