<?php

namespace App\Http\Controllers;

use App\Pelanggan as model_pelanggan;
use Illuminate\Http\Request;

class Pelanggan extends Controller
{
    public function InputPelanggan(){

        return view('pelanggan/input_pelanggan');
    }

    public function ProsesInput(Request $request){
        $data = [
            'nama_tujuan' => $request->nama_tujuan,
            'kota_tujuan' => $request->kota_tujuan,
            'prov_tujuan' => $request->prov_tujuan,
            'telp_tujuan' => $request->telp_tujuan,
            'email_tujuan' => $request->email_tujuan
        ];

        model_pelanggan::create($data);

        return redirect()->to('/pelanggan');
    }

    public function UbahPelanggan(model_pelanggan $pelanggan){

        return view('pelanggan/ubah_pelanggan', compact('pelanggan'));
    }

    public function ProsesUbah(Request $request, model_pelanggan $pelanggan){
        $data = [
            'nama_tujuan' => $request->nama_tujuan,
            'kota_tujuan' => $request->kota_tujuan,
            'prov_tujuan' => $request->prov_tujuan,
            'telp_tujuan' => $request->telp_tujuan,
            'email_tujuan' => $request->email_tujuan
        ];

        $pelanggan->update($data);

        return redirect()->to('/pelanggan');
    }

    public function ProsesHapus(model_pelanggan $pelanggan){
        $pelanggan->delete();

        return redirect()->to('/pelanggan');
    }
}
