<?php

namespace App\Http\Controllers;

use App\Supplier as model_supplier;
use Illuminate\Http\Request;

class Supplier extends Controller
{
    public function InputSupplier(){

        return view('supplier/input_supplier');
    }

    public function ProsesInput(Request $request){
        $data = [
            'nama_supplier' => $request->nama_supplier,
            'kota' => $request->kota,
            'provinisi' => $request->provinisi,
            'telp' => $request->telp,
            'email' => $request->email,
        ];

        model_supplier::create($data);

        return redirect()->to('/supplier');
    }

    public function UbahSupplier(model_supplier $supplier){

        return view('supplier/ubah_supplier', compact('supplier'));
    }

    public function ProsesUbah(Request $request, model_supplier $supplier){
        $data = [
            'nama_supplier' => $request->nama_supplier,
            'kota' => $request->kota,
            'provinisi' => $request->provinisi,
            'telp' => $request->telp,
            'email' => $request->email,
        ];

        $supplier->update($data);

        return redirect()->to('/supplier');
    }

    public function ProsesHapus(model_supplier $supplier){
        $supplier->delete();

        return redirect()->to('/supplier');
    }
}
