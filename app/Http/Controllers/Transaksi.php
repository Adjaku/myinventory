<?php

namespace App\Http\Controllers;

use App\Barang;
use App\Masuk;
use App\Keluar;
use Illuminate\Http\Request;

class Transaksi extends Controller
{
    public function TambahBarangMasuk(Request $request){
        $id_barang = $request->id_barang;
        $jumlah_barang = $request->jumlah_masuk;

        $data = [
            'id_supplier' => $request->id_supplier,
            'id_barang' => $request->id_barang,
            'jumlah_masuk' => $request->jumlah_masuk,
            'tanggal_masuk' => $request->tanggal_masuk
        ];

        Masuk::create($data);

        $barang = Barang::find($id_barang);
        $barang->jumlah += $jumlah_barang;
        $barang->save();


        return redirect()->to('/masuk');
    }

    public function TambahBarangKeluar(Request $request){
        $id_barang = $request->id_barang;
        $jumlah_barang = $request->jumlah_keluar;

        $data = [
            'id_tujuan' => $request->id_tujuan,
            'id_barang' => $request->id_barang,
            'jumlah_keluar' => $request->jumlah_keluar,
            'tanggal_keluar' => $request->tanggal_keluar,
        ];

        $barang = Barang::find($id_barang);
        if($barang->jumlah >= $jumlah_barang){
        Keluar::create($data);

        $barang->jumlah -= $jumlah_barang;
        $barang->save();
        }
        return redirect()->to('/keluar');
    }

}
