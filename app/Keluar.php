<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keluar extends Model
{
    protected $table = 'keluar';

    protected $fillable = [
        'id_tujuan','id_barang','jumlah_keluar','tanggal_keluar',
    ];
}
