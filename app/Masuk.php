<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Masuk extends Model
{
    protected $table = 'masuk';

    protected $fillable = [
        'id_supplier','id_barang','jumlah_masuk','tanggal_masuk',
    ];
}
