<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelanggan extends Model
{
    protected $table = 'tujuan';
    protected $primaryKey = 'id_tujuan';

    protected $fillable = [
        'nama_tujuan','kota_tujuan','prov_tujuan','telp_tujuan','email_tujuan',
    ];
}
