@extends('layouts.app')

@section('Title','Barang')
@section('Content')
<div class="container-fluid">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3">
                <a href="/barang_input" class="btn btn-block btn-info">Tambah Barang</a> <br>
            </div>
            <div class="col-12">
              <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Data Barang</h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <table id="barang" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                          <th>Nama Kategori</th>
                          <th>Nama Barang</th>
                          <th>Jumlah Barang</th>
                          <th>Satuan</th>
                          <th>Harga Beli</th>
                          <th>Harga Jual</th>
                          <th>Aksi</th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($barang as $data_barang)
                      <tr>
                        <td>{{ $data_barang->nama_kategori }}</td>
                        <td>{{ $data_barang->nama_barang }}</td>
                        <td>{{ $data_barang->jumlah }}</td>
                        <td>{{ $data_barang->satuan }}</td>
                        <td>{{ $data_barang->harga_beli }}</td>
                        <td>{{ $data_barang->harga_jual }}</td>
                        <th>
                          <div class="row">
                              <div class="col-sm">
                                  <a href="/barang_ubah/{{ $data_barang->id_barang }}"
                                      class="btn btn-sm btn-block m-0 btn-warning"><i class="far fa-edit"></i>
                                      Ubah</a>
                              </div>
                              <div class="col-sm">
                                  <form action="/barang_hapus/{{ $data_barang->id_barang }}" method="POST">
                                      @method('delete')
                                      @csrf
                                      <button type="submit" class="btn btn-sm btn-block m-0 btn-danger"><i class="far fa-trash-alt"></i>Hapus</button>
                                  </form>
                              </div>
                          </div>
                        </th>
                      </tr>
                      @endforeach
                      </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</div>
@endsection
@push('script-page')
<script>
$(document).ready( function () {
    $('#barang').DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#barang_wrapper .col-md-6:eq(0)');
} );
</script>
@endpush
