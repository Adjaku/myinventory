@extends('layouts.app')

@section('Title', 'Ubah Data Barang')
@section('Content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <!-- form start -->
                <form action="/barang_ubah/{{ $barang->id_barang }}" method="POST">
                  @method('patch')
                  @csrf
                  <div class="card-body">
                    <div class="form-group">
                        <label>Nama Kategori</label>
                        <select class="form-control select2" name="id_kategori" style="width: 100%;">
                          @foreach($kategori as $data_kategori)
                          <option value="{{ $data_kategori->id_kategori }}" {{ $data_kategori->id_kategori == $barang->id_kategori ? 'selected' : '' }}>{{ $data_kategori->nama_kategori }}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="nama_barang">Nama Barang</label>
                      <input type="text" class="form-control" id="nama_barang" name="nama_barang" value="{{ $barang->nama_barang }}" placeholder="Masukkan Nama Barang">
                    </div>
                    <div class="form-group">
                        <label for="jumlah_barang">Jumlah Barang</label>
                        <input type="text" class="form-control" id="jumlah_barang" name="jumlah" value="{{ $barang->jumlah }}" placeholder="Masukkan Jumlah Barang" readonly>
                    </div>
                    <div class="form-group">
                        <label for="satuan">Satuan</label>
                        <input type="text" class="form-control" id="satuan" name="satuan" value="{{ $barang->satuan }}" placeholder="Pilih Satuan">
                    </div>
                    <div class="form-group">
                        <label for="harga_beli">Harga Beli</label>
                        <input type="text" class="form-control" id="harga_beli" name="harga_beli" value="{{ $barang->harga_beli }}" placeholder="Masukkan Harga Beli">
                    </div>
                    <div class="form-group">
                        <label for="harga_jual">Harga Jual</label>
                        <input type="text" class="form-control" id="harga_jual" name="harga_jual" value="{{ $barang->harga_jual }}" placeholder="Masukkan Harga Jual">
                    </div>
                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Ubah</button>
                  </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
