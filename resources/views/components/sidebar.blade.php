<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
      <img src="dist/img/m.png" alt="MyInventory Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">MyInventory</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
                <a href="/" class="nav-link">
                  <i class="fas fa-tachometer-alt"></i>
                  <p>
                    &nbsp; &nbsp; &nbsp; Dashboard
                  </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="/kategori" class="nav-link">
                  <i class="fas fa-list"></i>
                  <p>
                    &nbsp; &nbsp; &nbsp; Kategori
                  </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="/barang" class="nav-link">
                  <i class="fas fa-boxes"></i>
                  <p>
                    &nbsp; &nbsp; &nbsp; Barang
                  </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="/supplier" class="nav-link">
                  <i class="fas fa-parachute-box"></i>
                  <p>
                    &nbsp; &nbsp; &nbsp; Supplier
                  </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="/pelanggan" class="nav-link">
                  <i class="fas fa-users"></i>
                  <p>
                    &nbsp; &nbsp; Pelanggan
                  </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-tachometer-alt"></i>
                  <p>
                    Transaksi
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="masuk" class="nav-link">
                      <i class="fas fa-shopping-cart"></i>
                      <p>Masuk</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="keluar" class="nav-link">
                      <i class="fas fa-money-bill-wave"></i>
                      <p>Keluar</p>
                    </a>
                  </li>
                </ul>
            </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
