@extends('layouts.app')

@section('Title','Dashboard')
@section('Content')
    <div class="container-fluid">

        {{-- box awal --}}
        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-info elevation-1"><i class="fas fa-boxes"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Jumlah Barang</span>
                    <span class="info-box-number">
                      {{ count($barang) }}
                    </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                  <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-parachute-box"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Jumlah Supplier</span>
                    <span class="info-box-number">{{ count($supplier) }}</span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                  <span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Jumlah Pelanggan</span>
                    <span class="info-box-number">{{ count($pelanggan) }}</span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                  <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-shopping-cart"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Jumlah Transaksi</span>
                    <span class="info-box-number">{{ count($masuk)+count($keluar) }}</span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
        </div>

        {{-- Data Table --}}
        <div class="container-fluid">
            <div class="row">
              <div class="col-12">
                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Data Barang</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                      <table id="dashboard" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Id Barang</th>
                            <th>Nama Barang</th>
                            <th>Harga Masuk</th>
                            <th>Harga Keluar</th>
                            <th>Jumlah</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($barang as $data_barang)
                        <tr>
                          <td>{{ $data_barang->id_barang }}</td>
                          <td>{{ $data_barang->nama_barang }}</td>
                          <td>{{ $data_barang->harga_beli }}</td>
                          <td>{{ $data_barang->harga_jual }}</td>
                          <td>{{ $data_barang->jumlah }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-page')
<script>
$(document).ready( function () {
    $('#dashboard').DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#dashboard_wrapper .col-md-6:eq(0)');
} );
</script>
@endpush
