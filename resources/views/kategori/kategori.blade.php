@extends('layouts.app')

@section('Title','Kategori')
@section('Content')
{{-- {{ dd($kategori) }} --}}
<div class="container-fluid">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3">
                <a href="/kategori_input" class="btn btn-block btn-info">Tambah Kategori</a> <br>
            </div>
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Data Kategori Barang</h3>
                </div>
                <!-- /.card-header -->

                <div class="card-body">
                  <table id="kategori" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Nama Kategori</th>
                        <th>Keterangan</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($kategori as $data_kategori)
                    <tr>
                      <td>{{ $data_kategori->nama_kategori }}</td>
                      <td>{{ $data_kategori->keterangan }}</td>
                      <th>
                        <div class="row">
                            <div class="col-sm">
                                <a href="/kategori_ubah/{{ $data_kategori->id_kategori }}"
                                    class="btn btn-sm btn-block m-0 btn-warning"><i class="far fa-edit"></i>
                                    Ubah
                                </a>
                            </div>
                            <div class="col-sm">
                                <form action="kategori_hapus/{{ $data_kategori->id_kategori }}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-sm btn-block m-0 btn-danger"><i class="far fa-trash-alt"></i> Hapus</button>
                                </form>
                            </div>
                        </div>
                      </th>
                    </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</div>
@endsection
@push('script-page')
<script>
$(document).ready( function () {
    $('#kategori').DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#kategori_wrapper .col-md-6:eq(0)');
} );
</script>
@endpush
