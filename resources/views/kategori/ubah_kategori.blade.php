@extends('layouts.app')

@section('Title', 'Ubah Data Kategori')
@section('Content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <!-- form start -->
                <form action="/kategori_ubah/{{ $kategori->id_kategori }}" method="POST">
                  @method('patch')
                  @csrf
                  <div class="card-body">
                    <div class="form-group">
                      <label for="nama_kategori">Nama Kategori</label>
                      <input type="text" class="form-control" id="nama_kategori" name="nama_kategori" value="{{ $kategori->nama_kategori }}" placeholder="Masukkan Nama Kategori Barang">
                    </div>
                    <div class="form-group">
                      <label for="keterangan">Keterangan</label>
                      <input type="text" class="form-control" id="keterangan" name="keterangan" value="{{ $kategori->keterangan }}" placeholder="Keterangan Kategori">
                    </div>
                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Ubah</button>
                  </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
