@extends('layouts.app')

@section('Title','Pelanggan')
@section('Content')
<div class="container-fluid">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3">
                <a href="/pelanggan_input" class="btn btn-block btn-info">Tambah Pelanggan</a> <br>
            </div>
            <div class="col-12">
              <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Data Tujuan Pengiriman</h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <table id="tujuan" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>Nama Tujuan</th>
                        <th>Provinsi</th>
                        <th>Kota</th>
                        <th>No Telepon</th>
                        <th>Email</th>
                        <th>Aksi</th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($pelanggan as $data_pelanggan)
                      <tr>
                        <td>{{ $data_pelanggan->nama_tujuan }}</td>
                        <td>{{ $data_pelanggan->prov_tujuan }}</td>
                        <td>{{ $data_pelanggan->kota_tujuan }}</td>
                        <td>{{ $data_pelanggan->telp_tujuan }}</td>
                        <td>{{ $data_pelanggan->email_tujuan }}</td>
                        <th>
                          <div class="row">
                              <div class="col-sm">
                                  <a href="/pelanggan_ubah/{{ $data_pelanggan->id_tujuan }}"
                                      class="btn btn-sm btn-block m-0 btn-warning"><i class="far fa-edit"></i>
                                      Ubah</a>
                              </div>
                              <div class="col-sm">
                                  <form action="pelanggan_hapus/{{ $data_pelanggan->id_tujuan }}" method="POST">
                                      @method('delete')
                                      @csrf
                                      <button type="submit" class="btn btn-sm btn-block m-0 btn-danger"><i class="far fa-trash-alt"></i>Hapus</button>
                                  </form>
                              </div>
                          </div>
                        </th>
                      </tr>
                      @endforeach
                      </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</div>
@endsection
@push('script-page')
<script>
    $(document).ready( function () {
        $('#tujuan').DataTable({
          "responsive": true, "lengthChange": false, "autoWidth": false,
          "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#tujuan_wrapper .col-md-6:eq(0)');
    } );
    </script>
@endpush

