@extends('layouts.app')

@section('Title', 'Ubah Data Pelanggan')
@section('Content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <!-- form start -->
                <form action="/pelanggan_ubah/{{ $pelanggan->id_tujuan }}" method="POST">
                    @method('patch')
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="nama_tujuan">Nama Tujuan</label>
                            <input type="text" class="form-control" id="nama_tujuan" name="nama_tujuan" value="{{ $pelanggan->nama_tujuan }}" placeholder="Masukkan Nama Tujuan">
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="prov">Provinsi</label>
                                <input type="text" class="form-control" id="prov" name="prov_tujuan" value="{{ $pelanggan->prov_tujuan }}" placeholder="Masukkan Nama Provinsi">
                            </div>
                            <div class="form-group col-md-8">
                                <label for="kota">Kota</label>
                                <input type="text" class="form-control" id="kota" name="kota_tujuan" value="{{ $pelanggan->kota_tujuan }}" placeholder="Masukkan Nama Kota">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="telepon">No Telepon</label>
                                <input type="text" class="form-control" id="telepon" name="telp_tujuan" value="{{ $pelanggan->telp_tujuan }}" placeholder="08**********">
                            </div>
                            <div class="form-group col-md-8">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email_tujuan" value="{{ $pelanggan->email_tujuan }}" placeholder="example@gmail.com">
                            </div>
                        </div>
                      </div>
                      <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Ubah</button>
                  </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
