@extends('layouts.app')

@section('Title','Supplier')
@section('Content')
<div class="container-fluid">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3">
                <a href="/supplier_input" class="btn btn-block btn-info">Tambah Supplier</a> <br>
            </div>
            <div class="col-12">
              <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Data Supplier</h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <table id="supplier" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>Nama Supplier</th>
                        <th>Provinsi</th>
                        <th>Kota</th>
                        <th>No Telepon</th>
                        <th>Email</th>
                        <th>Aksi</th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($supplier as $data_supplier)
                      <tr>
                        <td>{{ $data_supplier->nama_supplier }}</td>
                        <td>{{ $data_supplier->provinisi }}</td>
                        <td>{{ $data_supplier->kota }}</td>
                        <td>{{ $data_supplier->telp }}</td>
                        <td>{{ $data_supplier->email }}</td>
                        <th>
                          <div class="row">
                              <div class="col-sm">
                                  <a href="/supplier_ubah/{{ $data_supplier->id_supplier }}"
                                      class="btn btn-sm btn-block m-0 btn-warning"><i class="far fa-edit"></i>
                                      Ubah</a>
                              </div>
                              <div class="col-sm">
                                  <form action="supplier_hapus/{{ $data_supplier->id_supplier }}" method="POST">
                                      @method('delete')
                                      @csrf
                                      <button type="submit" class="btn btn-sm btn-block m-0 btn-danger"><i class="far fa-trash-alt"></i>Hapus</button>
                                  </form>
                              </div>
                          </div>
                        </th>
                      </tr>
                      @endforeach
                      </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</div>
@endsection
@push('script-page')
<script>
    $(document).ready( function () {
        $('#supplier').DataTable({
          "responsive": true, "lengthChange": false, "autoWidth": false,
          "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#supplier_wrapper .col-md-6:eq(0)');
    } );
    </script>
@endpush

