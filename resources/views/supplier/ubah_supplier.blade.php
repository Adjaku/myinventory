@extends('layouts.app')

@section('Title', 'Ubah Data Supplier')
@section('Content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <!-- form start -->
                <form action="/supplier_ubah/{{ $supplier->id_supplier }}" method="POST">
                    @method('patch')
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="nama_supplier">Nama Supplier</label>
                            <input type="text" class="form-control" id="nama_supplier" name="nama_supplier" value="{{ $supplier->nama_supplier }}" placeholder="Masukkan Nama Supplier">
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="prov">Provinsi</label>
                                <input type="text" class="form-control" id="prov" name="provinisi" value="{{ $supplier->provinisi }}" placeholder="Masukkan Nama Provinsi">
                            </div>
                            <div class="form-group col-md-8">
                                <label for="kota">Kota</label>
                                <input type="text" class="form-control" id="kota" name="kota" value="{{ $supplier->kota }}" placeholder="Masukkan Nama Kota">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="telepon">No Telepon</label>
                                <input type="text" class="form-control" id="telepon" name="telp" value="{{ $supplier->telp }}" placeholder="08**********">
                            </div>
                            <div class="form-group col-md-8">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" value="{{ $supplier->email }}" placeholder="example@gmail.com">
                            </div>
                        </div>
                      </div>
                      <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Ubah</button>
                  </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
