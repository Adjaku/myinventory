@extends('layouts.app')

@section('Title','Barang Keluar')
@section('Content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <!-- form start -->
                <form action="/keluar" method="POST">
                  @csrf
                  <div class="card-body">
                    <div class="form-group">
                        <label>Nama Pelanggan</label>
                        <select class="form-control select2" name="id_tujuan" style="width: 100%;">
                            @foreach($pelanggan as $data_pelanggan)
                                <option value="{{ $data_pelanggan->id_tujuan }}">{{ $data_pelanggan->nama_tujuan }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="nama_barang">Nama Barang</label>
                      <select class="form-control select2" name="id_barang" style="width: 100%;">
                        @foreach($barang as $data_barang)
                            <option value="{{ $data_barang->id_barang }}">{{ $data_barang->nama_barang }}</option>
                        @endforeach
                    </select>
                    </div>
                    <div class="form-group">
                        <label for="jumlah_barang">Jumlah Barang</label>
                        <input type="text" class="form-control" name="jumlah_keluar" placeholder="Masukkan Jumlah Barang">
                    </div>
                    <div class="form-group">
                        <label for="jumlah_barang">Tanggal Keluar</label>
                        <input type="date" class="form-control" name="tanggal_keluar">
                    </div>
                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
            </div>
        </div>
    </div>

    <table id="keluar" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Nama Tujuan</th>
            <th>Nama Barang</th>
            <th>Jumlah Masuk</th>
            <th>Tanggal Masuk</th>
        </tr>
        </thead>
        <tbody>
        @foreach($keluar as $data_keluar)
        <tr>
          <td>{{ $data_keluar->nama_tujuan }}</td>
          <td>{{ $data_keluar->nama_barang }}</td>
          <td>{{ $data_keluar->jumlah_keluar }}</td>
          <td>{{ $data_keluar->tanggal_keluar }}</td>
        </tr>
        @endforeach
        </tbody>
      </table>

@endsection
@push('script-page')
<script>
    $(document).ready( function () {
        $('#keluar').DataTable({
          "responsive": true, "lengthChange": false, "autoWidth": false,
          "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#keluar_wrapper .col-md-6:eq(0)');

        //Initialize Select2 Elements
        $('.select2').select2()

        //Initialize Select2 Elements
        $('.select2bs4').select2({
        theme: 'bootstrap4'
        })
    } );
    </script>
@endpush
