@extends('layouts.app')

@section('Title','Barang Masuk')
@section('Content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <!-- form start -->
                <form action="/masuk" method="POST">
                  @csrf
                  <div class="card-body">
                    <div class="form-group">
                        <label>Nama Supplier</label>
                        <select class="form-control select2" name="id_supplier" style="width: 100%;">
                            @foreach($supplier as $data_supplier)
                                <option value="{{ $data_supplier->id_supplier }}">{{ $data_supplier->nama_supplier }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="nama_barang">Nama Barang</label>
                      <select class="form-control select2" name="id_barang" style="width: 100%;">
                        @foreach($barang as $data_barang)
                            <option value="{{ $data_barang->id_barang }}">{{ $data_barang->nama_barang }}</option>
                        @endforeach
                    </select>
                    </div>
                    <div class="form-group">
                        <label for="jumlah_barang">Jumlah Barang</label>
                        <input type="text" class="form-control" name="jumlah_masuk" placeholder="Masukkan Jumlah Barang">
                    </div>
                    <div class="form-group">
                        <label for="jumlah_barang">Tanggal Masuk</label>
                        <input type="date" class="form-control" name="tanggal_masuk">
                    </div>
                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
            </div>
        </div>
    </div>

    <table id="masuk" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Nama Supplier</th>
            <th>Nama Barang</th>
            <th>Jumlah Masuk</th>
            <th>Tanggal Masuk</th>
        </tr>
        </thead>
        <tbody>
        @foreach($masuk as $data_masuk)
        <tr>
          <td>{{ $data_masuk->nama_supplier }}</td>
          <td>{{ $data_masuk->nama_barang }}</td>
          <td>{{ $data_masuk->jumlah_masuk }}</td>
          <td>{{ $data_masuk->tanggal_masuk }}</td>
        </tr>
        @endforeach
        </tbody>
      </table>
@endsection
@push('script-page')
<script>
    $(document).ready( function () {
        $('#masuk').DataTable({
          "responsive": true, "lengthChange": false, "autoWidth": false,
          "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#masuk_wrapper .col-md-6:eq(0)');

        //Initialize Select2 Elements
        $('.select2').select2()

        //Initialize Select2 Elements
        $('.select2bs4').select2({
        theme: 'bootstrap4'
        })
    } );
    </script>
@endpush
