<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'MyInventory@index');

Route::get('/barang', 'MyInventory@barang');
Route::get('/barang_input', 'Barang@InputBarang');
Route::get('/barang_ubah/{barang:id_barang}', 'Barang@UbahBarang');
Route::post('barang_input', 'Barang@ProsesInput');
Route::patch('barang_ubah/{barang:id_barang}', 'Barang@ProsesUbah');
Route::delete('barang_hapus/{barang:id_barang}', 'Barang@ProsesHapus');

Route::get('/supplier', 'MyInventory@supplier');
Route::get('/supplier_input', 'Supplier@InputSupplier');
Route::get('/supplier_ubah/{supplier:id_supplier}', 'Supplier@UbahSupplier');
Route::post('supplier_input', 'Supplier@ProsesInput');
Route::patch('supplier_ubah/{supplier:id_supplier}', 'Supplier@ProsesUbah');
Route::delete('supplier_hapus/{supplier:id_supplier}', 'Supplier@ProsesHapus');

Route::get('/pelanggan', 'MyInventory@pelanggan');
Route::get('/pelanggan_input', 'Pelanggan@InputPelanggan');
Route::get('/pelanggan_ubah/{pelanggan:id_tujuan}', 'Pelanggan@UbahPelanggan');
Route::post('pelanggan_input', 'Pelanggan@ProsesInput');
Route::patch('pelanggan_ubah/{pelanggan:id_tujuan}', 'Pelanggan@ProsesUbah');
Route::delete('pelanggan_hapus/{pelanggan:id_tujuan}', 'Pelanggan@ProsesHapus');

Route::get('/keluar', 'MyInventory@keluar');
Route::post('keluar', 'Transaksi@TambahBarangKeluar');

Route::get('/masuk', 'MyInventory@masuk');
Route::post('masuk', 'Transaksi@TambahBarangMasuk');

Route::get('/kategori', 'MyInventory@kategori');
Route::get('/kategori_input', 'Kategori@InputKategori');
Route::get('/kategori_ubah/{kategori:id_kategori}', 'Kategori@UbahKategori');
Route::post('kategori_input', 'Kategori@ProsesInput');
Route::patch('kategori_ubah/{kategori:id_kategori}', 'Kategori@ProsesUbah');
Route::delete('kategori_hapus/{kategori:id_kategori}', 'Kategori@ProsesHapus');


